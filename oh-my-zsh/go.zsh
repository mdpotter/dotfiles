export GOPATH=${HOME}/code/go
export PATH=${PATH}:/usr/local/go/bin:${HOME}/code/go/bin
#export GO15VENDOREXPERIMENT=1

alias gcov='gocov test | gocov-html > ~/Desktop/coverage.html && open ~/Desktop/coverage.html'
