#!/usr/bin/env zsh

# Symlink files
p=${0:a:h}
if (( ${+ZSH} )); then
    for file (${p}/*.zsh(N)); do
      ln -nsf ${file} ${ZSH}/custom/.
    done
else
    print "ohmyzsh doesn't seem to be loaded"
fi
unset file
