# Alias zcat for OS X
if [ "$(uname)" = "Darwin" ]; then
    alias zcat='gzcat'
    alias ll='ls -lFbh --color=always'
    alias la='ls -alFbh --color=always'
elif [ "$(expr substr $(uname -s) 1 5)" = "Linux" ]; then
    alias ll='ls -lFbh --color=always'
    alias la='ls -alFbh --color=always'
fi

alias vimr='open -a VimR'
alias gomp='cd ~/code/go/src/github.com/mikepotter'
alias gompp='cd ~/code/go/src/bitbucket.org/mdpotter'
alias k='kubectl'
