#!/usr/bin/env bash
HOME=/Users/mike
NAME=BadWolf
SMALLNAME=$(echo $NAME | tr '[:upper:]' '[:lower:]')

#if ! test "$(scutil --get ComputerName)" = "$NAME"; then
#  echo "Setting Computer Name"
#  sudo scutil --set ComputerName $NAME
#  sudo scutil --set HostName $SMALLNAME 
#  sudo scutil --set LocalHostName $SMALLNAME
#  sudo defaults write /Library/Preferences/SystemConfiguration/com.apple.smb.server NetBIOSName -string $SMALLNAME
#fi

if ! test -f "$HOME/.ssh/id_rsa"; then
  echo "Creating an SSH key for you..."
  ssh-keygen -t rsa -C "badwolf@mikenbob.com"

  echo "Please add this public key to Github \n"
  echo "https://github.com/account/ssh \n"
  read -p "Press [Enter] key after this..."
fi

# Install dev tools, make sure to complete the prompt
if ! test $(xcode-select -p); then
  xcode-select --install
  echo "Complete xcode install"
  read -p "Press [Enter] key after this..."
fi

# Install Homebrew,
if ! test -f "/opt/homebrew/bin/brew"; then
  echo "Installing homebrew..."
  /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
fi

if test ! $(which brew); then
  eval $(/opt/homebrew/bin/brew shellenv)
fi


# Update homebrew recipes
echo "Updating homebrew..."
brew update

#echo "Installing brew applications"
#brew bundle
#brew cleanup

if ! test -f "$HOME/.oh-my-zsh/oh-my-zsh.sh"; then
  echo "Installing OhMyZSH"
  sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
fi

#if [ $SHELL != "/bin/zsh" ]; then
#  echo "Setting ZSH as shell..."
#  chsh -s /bin/zsh
#fi


#rbenv install 3.2.2
#rbenv global 3.2.2

# Defaults
defaults -currentHost write com.apple.controlcenter.plist Bluetooth -int 18 # Show bluetooth in menu bar
defaults -currentHost write com.apple.menuextra.clock IsAnalog -bool true # Set clock to analog
defaults -currentHost write com.apple.Spotlight MenuItemHidden -int 1 # Hide spotlight from meny bar

