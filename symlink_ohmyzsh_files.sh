#!/bin/bash

for FILE in gemrc vimrc gvimrc gitconfig zshrc vim ansible.cfg screenrc lessfilter; do
    ln -nsf $HOME/.dotfiles/$FILE $HOME/.$FILE
done
