#!/bin/bash

for FILE in vimrc gitconfig vim screenrc lessfilter; do
    ln -nsf $HOME/.dotfiles/$FILE $HOME/.$FILE
done
